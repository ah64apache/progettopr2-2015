/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unica.pr2.progetto2015.g48911_49269;

import it.unica.pr2.progetto2015.interfacce.SheetFunction;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Andrea-Hicham
 */
public class Custom implements SheetFunction {

    
    
    @Override
    public Object execute(Object... args) {
        Date data = new Date();
        data.setTime(Long.valueOf((String)args[0]));
        
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        //System.out.println(format1.format(data));
        System.out.println("Progetto2015: Executing Custom");
        
        return getJson(format1.format(data));
    }

  /**
   * 
   * @return Restituisce la categoria di appartenenza della funzione
   */
    @Override
    public String getCategory() {
        return "Custom";
    }
/**
 * 
 * @return Restituisce una stringa di spiegazione
 */
    @Override
    public String getHelp() {
         return "Fissata una data restituisce le informazioni metereologiche osservate dal rover curiosity sul pianeta Marte.";
    }

    /**
     *
     * @return il nome della funzione
     */
    @Override
    public String getName() {
        return "CURIOSITY.INFO";
    }

    /**
     * 
     * @param data La data per cui si vogliono ricevere le informazioni
     * @return Restituisce una matrice con il nome dei parametri e i rispettivi valori
     */
    private String[][] getJson(String data) {
        String res="";
        try {
            URL ulr = new URL("http://marsweather.ingenology.com/v1/archive/?terrestrial_date=" + data + "&format=json");
            URLConnection urlConnection = ulr.openConnection();
            BufferedReader reader = null;

            reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            
            String inputLine;
            while ((inputLine = reader.readLine()) != null) {
                res+=inputLine;
                //System.out.println(inputLine);//DEBUG
            }
            
            JSONObject json = new JSONObject (res);
            
            if (json.getInt("count")==0){
                return new String[][]{{"Dato non trovato per la data selezionata ",""}};
            }
            JSONArray a = json.getJSONArray("results");
            //System.out.println(a);//DEBUG
            JSONObject arr = a.getJSONObject(0);
            
            
            String [] sol;
            if(!arr.isNull("sol"))
                sol = new String[]{"Giorni passati su marte: ",String.valueOf(arr.getInt("sol"))};
            else{
                sol = new String[]{"Giorni passati su marte: ","Dato non disponibile"};
            }
            
            String [] ls;
            if(!arr.isNull("ls"))
                ls = new String[]{"Stagione: ",getNomeStagione(arr.getDouble("ls"))};
            else{
                ls = new String[]{"Stagione: ","Dato non disponibile"};
            }
            
            String [] temperaturaMin;
            if(!arr.isNull("min_temp"))
                temperaturaMin = new String[]{"Temp. minima: ",String.valueOf(arr.getDouble("min_temp"))};
            else{
                temperaturaMin = new String[]{"Temp. minima: ","Dato non disponibile"};
            }
            String [] temperaturaMax;
            if(!arr.isNull("max_temp"))
                temperaturaMax = new String[]{"Temp. massima: ",String.valueOf(arr.getDouble("max_temp"))};
            else{
                temperaturaMax = new String[]{"Temp. massima: ","Dato non disponibile"};
            }
            String [] pressione;
            if(!arr.isNull("pressure"))
                pressione = new String[]{"Pressione: ",String.valueOf(arr.getDouble("pressure"))};
            else{
                pressione = new String[]{"Pressione: ","Dato non disponibile"};
            }
            
            
            
            
            String [] stringaPressione = new String[]{"Pressione: ",arr.getString("pressure_string")};
            
            
            
            String [] umidita;
            if(!arr.isNull("abs_humidity"))
                umidita = new String[]{"Umidità: ",String.valueOf(arr.getInt("abs_humidity"))};
            else{
                umidita = new String[]{"Umidità: ","Dato non disponibile"};
            }
            
            String [] velocitaVento;
            if(!arr.isNull("wind_speed"))
                velocitaVento = new String[]{"Velocità del vento: ",String.valueOf(arr.getDouble("wind_speed"))};
            else{
                velocitaVento = new String[]{"Velocità del vento: ","Dato non disponibile"};
            }
            String [] direzioneVento;
            if(!arr.isNull("wind_direction"))
                direzioneVento = new String[]{"Direzione del vento: ",arr.getString("wind_direction")};
             else{
                direzioneVento = new String[]{"Direzione del vento: ","Dato non disponibile"};
            }
            String [] meteo;
            if(!arr.isNull("atmo_opacity")){
              meteo = new String[]{"Meteo: ",arr.getString("atmo_opacity")};
             }else{
             meteo= new String[]{"Meteo: ","Dato non disponibile"};
             }
            String [] stagione;
            if(!arr.isNull("season")){
              stagione = new String[]{"Stagione: ",arr.getString("season")};
             }else{
             stagione= new String[]{"Stagione: ","Dato non disponibile"};
             }
            String [] alba;
            if(!arr.isNull("sunrise")){
              alba = new String[]{"Alba: ",arr.getString("sunrise")};
             }else{
             alba= new String[]{"Alba: ","Dato non disponibile"};
             }
            String [] tramonto;
            if(!arr.isNull("sunset")){
              tramonto = new String[]{"Tramonto: ",arr.getString("sunset")};
             }else{
             tramonto= new String[]{"Tramonto: ","Dato non disponibile"};
             }
            
            return new String[][]{sol,ls,temperaturaMin,temperaturaMax,pressione,stringaPressione,umidita,velocitaVento,direzioneVento,meteo,stagione,alba,tramonto};
            
            
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     * Data una valoree per la longitudine di marte restituisce il nome della rispettiva stagione
     * @param longitudineSolare
     * @return stagione
     */
    
    private String getNomeStagione(double longitudineSolare){
    String stato="";
                if(0==longitudineSolare){
                    stato = "Equinozio di primavera (Emisfero Nord)";
                }else if(longitudineSolare>0&&longitudineSolare<60){
                     stato = "Primavera (Emisfero Nord)";
                }
                else if(longitudineSolare>=60&&longitudineSolare<90){
                    if(longitudineSolare==71){
                        stato="Primavera ( Afelio, massima distanza di marte dal sole )";
                    
                    }else
                     stato = "Primavera (Emisfero Nord)";
                }
                else if(longitudineSolare>=90&&longitudineSolare<180){
                    if(longitudineSolare==90){
                        stato="Solstizio d\'estate (Emisfero Nord)";
                    }
                     stato = "Estate";
                }
                else{
                    if(longitudineSolare==180){
                        stato = "Equinozio d\'autunno (Emisfero nord). Inizio della stagione delle tempeste di sabbia";
                    }else if(longitudineSolare==251){
                         stato = "Perielio. longitudineSolare delle tempeste di sabbia";
                    
                    }
                    else if(longitudineSolare==270){
                         stato = "Solstizio d\'inverno (Emisfero Nord). Stagione delle tempeste di sabbia";
                    
                    }
                    else {
                         stato = "Stagione delle tempeste di sabbia";
                    
                    }
    
    }
                return stato;
}
}
