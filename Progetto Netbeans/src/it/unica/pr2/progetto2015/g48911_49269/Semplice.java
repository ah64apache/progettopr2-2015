/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unica.pr2.progetto2015.g48911_49269;

import it.unica.pr2.progetto2015.interfacce.SheetFunction;

/**
 *
 * @author Andrea-Hicham
 */

public class Semplice implements SheetFunction {

    double valore;

    public Semplice() {
        super();
    }

    @Override
    public Object execute(Object... args) {
        System.out.println("Progetto2015: Executing Semplice");
        Double res = Math.pow((Double) args[0], (Double) args[1]);
        return res;
    }

    /**
     *
     * @return Restituisce la categoria di appartenenza della funzione
     */
    @Override
    public String getCategory() {
        return "matematica"; 
    }

    /**
     *
     * @return Restituisce una stringa di spiegazione
     */
    @Override
    public String getHelp() {
        return "Fornire in input il valore da elevare a potenza e l\'esponente,\nresituisce in output il risultato dell\'elevazione a potenza";
    }

    /**
     *
     * @return il nome della funzione
     */
    @Override
    public String getName() {
        return "POTENZA";
    }

}
