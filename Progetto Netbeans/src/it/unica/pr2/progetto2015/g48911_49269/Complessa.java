/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unica.pr2.progetto2015.g48911_49269;

import it.unica.pr2.progetto2015.interfacce.SheetFunction;

/**
 *
 * @author Andrea-Hicham
 */
public class Complessa implements SheetFunction{


    @Override
    public Object execute(Object... args) {
        
        System.out.println("Progetto2015: Executing Complessa");
          Double[][] res;
          res=prodotto((Double[][])args[0],(Double[][])args[1]);
       
          return res;
    }
    
      /**
     * Calcola la matrice prodotto
     * @param a Prima matrice
     * @param b Seconda matrice
     * @return la matrice prodotto o una matrice di 0 se impossibile
     */
 public  Double[][] prodotto(Double[][] a, Double[][] b) {

  double[][] matrice;
  Double [][] ret = null;
  if (a[0].length != b.length){
   ret = new Double[1][1];
   ret[0][0]=0.0;
  }
      else {
       matrice = new double[a.length][b[0].length];
       ret = new Double[a.length][b[0].length];
       for(int i=0; i<matrice.length; i++)
        for(int j=0; j<matrice[0].length; j++)
         for(int k=0; k<b.length; k++) {
           matrice[i][j] = matrice[i][j] + a[i][k]*b[k][j];
           ret[i][j]= new Double(matrice[i][j]);
         }
      }
  
  
  
      return ret;
  }
  
 

   

    @Override
    public String getCategory() {
        return "MATRICE"; 
    }

    @Override
    public String getHelp() {
        return "Date due matrici ne restituisce il prodotto."; 
    }

    @Override
    public String getName() {
        return "MATRICE PRODOTTO"; 
    }
    
    
    
}
