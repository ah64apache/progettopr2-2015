/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unica.pr2.progetto2015.g48911_49269;

import it.unica.pr2.progetto2015.interfacce.SheetFunction;

/**
 *
* @author Andrea-Hicham
 */

public class Semplice implements SheetFunction {

    double valore;

    public Semplice() {
        super();
    }

    @Override
    public Object execute(Object[] args) {
        System.out.println("Progetto2015: Executing Semplice");
        return Math.pow((double) args[0], (double) args[1]);
    }

    /**
     *
     * @return Restituisce la categoria di appartenenza della funzione
     */
    @Override
    public String getCategory() {
        return "MATEMATICA"; //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @return Restituisce una stringa di spiegazione
     */
    @Override
    public String getHelp() {
        return "Fornire in input il valore da elevare a potenza e l\'esponente,\nresituisce in output il risultato dell\'elevazione a potenza";
    }

    /**
     *
     * @return il nome della funzione
     */
    @Override
    public String getName() {
        return "POTENZA";
    }

}
