*Progetto finale PR2 2015*
==========================

## Il progetto si compone di *3 classi* che implementano la stessa interfaccia SheetFunction. ##

1. La classe **Semplice** implementa la funzione di Potenza, forniti un valore e un esponente calcola val^esp.

2. La classe **Complessa** implementa la funzione MATR.PRODOTTO, date due matrici ne restituisce il prodotto.

3. La classe **Custom** implementa una funzione che, stabilita una data visualizza i dati metereoligici di marte forniti dal rover Curiosity. Il parsing dei dati è effettuato grazie alla libreria esterna **org.json** mentre il data source è fornito dal **{MAAS} Mars Atmospheric Aggregation System**.

La cartella Excel è divisa in 3 fogli.

1. Nel primo è implementata la funzione Semplice

2. Nel secondo è implementata la funzione Complessa

3. Nel terzo è implementata la funzione Custom